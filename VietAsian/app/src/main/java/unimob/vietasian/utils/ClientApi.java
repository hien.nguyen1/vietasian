package unimob.vietasian.utils;

import unimob.vietasian.vietAsianServiceRepositories.VietAsianService;

public class ClientApi extends BaseApi {
    public VietAsianService unideliService(){
        return this.getService(VietAsianService.class,ConfigApi.BASE_URL);
    }

//    public AuthService authService(){
//        return this.getService(AuthService.class,ConfigApi.BASE_URL);
//    }
}
